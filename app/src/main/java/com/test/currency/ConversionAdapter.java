package com.test.currency;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.currency.model.Conversion;

import java.util.List;


public class ConversionAdapter extends RecyclerView.Adapter<ConversionAdapter.ConversionHolder> {
    private List<Conversion> conversions;

    ConversionAdapter(Context context, List<Conversion> conversions) {
        this.conversions = conversions;
    }

    @NonNull
    @Override
    public ConversionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ConversionHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(android.R.layout.two_line_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ConversionHolder viewHolder, int i) {

        Conversion conversion = conversions.get(i);
        viewHolder.baseTextView.setText(format(conversion.getFromCurrency(), String.valueOf(conversion.getFromValue())));
        viewHolder.rateTextView.setText(format(conversion.getToCurrency(), String.valueOf(conversion.getToValue())));
    }

    @Override
    public int getItemCount() {
        return conversions != null ? conversions.size() : 0;
    }

    private String format(String base, String rate) {
        return String.format("%s : %s", base, rate);
    }

    void setConversions(List<Conversion> conversions) {
        this.conversions = conversions;
        notifyDataSetChanged();
    }

    class ConversionHolder extends RecyclerView.ViewHolder {

        TextView baseTextView;
        TextView rateTextView;

        ConversionHolder(View itemView) {
            super(itemView);
            baseTextView = itemView.findViewById(android.R.id.text1);
            rateTextView = itemView.findViewById(android.R.id.text2);
        }
    }
}