package com.test.currency;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.test.currency.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, LoginActivity.class);
        boolean logged;
        SharedPreferences preferences = getSharedPreferences("init", Context.MODE_PRIVATE);
        if (preferences != null) {
            logged = preferences.getBoolean("logged", false);
            if (logged)
                intent.setClass(this, ConverterActivity.class);
        }

        startActivity(intent);
        finish();
    }
}
