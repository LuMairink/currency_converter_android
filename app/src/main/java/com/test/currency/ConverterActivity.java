package com.test.currency;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.test.currency.api.CurrencyApi;
import com.test.currency.model.Conversion;
import com.test.currency.model.ConversionDao;
import com.test.currency.model.ConversionDb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

public class ConverterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private String from;
    private TextView toTextView;
    private EditText fromEditText;
    private String fromCurrency;
    private String toCurrency;
    private ConversionDb conversionDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);


        final Spinner fromSpinner = findViewById(R.id.from_currency);
        final Spinner toSpinner = findViewById(R.id.to_currency);
        final Button convertButton = findViewById(R.id.convert_button);
        fromEditText = findViewById(R.id.from_value);
        toTextView = findViewById(R.id.to_value);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.currencies, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        fromSpinner.setAdapter(adapter);
        toSpinner.setAdapter(adapter);

        fromSpinner.setOnItemSelectedListener(this);
        toSpinner.setOnItemSelectedListener(this);
        convertButton.setOnClickListener(this);
        conversionDb = ((CurrencyApplication) getApplication()).getConversionDb();

        SharedPreferences preferences = getSharedPreferences("init", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("logged", true);
        editor.apply();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("logged", true);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.history) {
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        toTextView.setText("");
        int viewId = parent.getId();

        if (viewId == R.id.from_currency) {
            fromCurrency = parent.getItemAtPosition(position).toString();
        }
        if (viewId == R.id.to_currency) {
            toCurrency = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void convert() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.exchangeratesapi.io")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Map<String, String> params = new HashMap<>();
        params.put("base", fromCurrency);
        params.put("symbols", toCurrency);

        CurrencyApi currencyApi = retrofit.create(CurrencyApi.class);
        Call<ResponseBody> call = currencyApi.getRate(params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            @EverythingIsNonNull
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(ConverterActivity.this, "Erro na requisição", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    if (response.body() != null) {
                        ResponseBody body = response.body();
                        JsonObject json = new Gson().fromJson(body.string(), JsonObject.class);
                        JsonObject rates = new Gson().fromJson(json.get("rates").toString(), JsonObject.class);
                        double rate = rates.get(toCurrency).getAsDouble();
                        String date = json.get("date").getAsString();
                        double baseValue = Double.valueOf(fromEditText.getText().toString());
                        double convertedValue = baseValue * rate;

                        toTextView.setText(String.valueOf(convertedValue));

                        final Conversion conversion = new Conversion();
                        conversion.setFromCurrency(fromCurrency);
                        conversion.setFromValue(baseValue);
                        conversion.setToCurrency(toCurrency);
                        conversion.setToValue(convertedValue);
                        conversion.setDate(date);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                conversionDb.daoAccess().insertConversion(conversion);
                            }
                        }).start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("error", t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (fromEditText.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Preencha o tipo de moeda", Toast.LENGTH_SHORT).show();
            return;
        }
        convert();
    }
}
