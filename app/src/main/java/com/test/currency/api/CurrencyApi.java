package com.test.currency.api;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface CurrencyApi {
    @GET("/latest")
    Call<ResponseBody> getRate(@QueryMap Map<String, String> options);
}
