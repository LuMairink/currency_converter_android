package com.test.currency.model;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Conversion.class}, version = 1, exportSchema = false)
public abstract class ConversionDb extends RoomDatabase {
    public abstract ConversionDao daoAccess();
}