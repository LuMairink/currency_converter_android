package com.test.currency.model;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;

import java.util.List;

@Dao
public interface ConversionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConversion(Conversion conversion);

    @Query("SELECT * from Conversion")
    List<Conversion> getRates();
}
