package com.test.currency;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.test.currency.model.Conversion;
import com.test.currency.model.ConversionDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HistoryActivity extends AppCompatActivity {

    private RecyclerView conversionList;
    private ConversionAdapter adapter;
    private List<Conversion> conversions = new ArrayList<>();
    private ConversionDb conversionDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        conversionList = findViewById(R.id.history_recyclerview);
        conversionDb = ((CurrencyApplication) getApplication()).getConversionDb();
        setupRecycler();
    }

    private void setupRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        conversionList.setLayoutManager(layoutManager);
        conversionList.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapter = new ConversionAdapter(HistoryActivity.this, conversions);
        conversionList.setAdapter(adapter);

        new Thread(new Runnable() {
            @Override
            public void run() {
                conversions = conversionDb.daoAccess().getRates();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setConversions(conversions);
                    }
                });
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setAdapter(ConversionAdapter adapter) {
        this.adapter = adapter;
        conversionList.setAdapter(adapter);
    }

    public void setConversions(List<Conversion> conversions) {
        this.conversions = conversions;
        adapter.setConversions(conversions);
    }
}
