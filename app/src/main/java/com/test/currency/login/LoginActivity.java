package com.test.currency.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.test.currency.ConverterActivity;
import com.test.currency.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button loginFacebookButton = findViewById(R.id.login_facebook);
        final Button loginTwitterButton = findViewById(R.id.login_twitter);
        final Button loginGoogleButton = findViewById(R.id.login_google);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                if (!username.equals("") && !password.equals("")) changeActivity();
                else showLoginFailed();
            }
        });
        loginFacebookButton.setOnClickListener(this);
        loginTwitterButton.setOnClickListener(this);
        loginGoogleButton.setOnClickListener(this);

    }

    private void showLoginFailed() {
        Toast.makeText(getApplicationContext(), "Preencha todos os campos.", Toast.LENGTH_SHORT).show();
    }

    private void changeActivity() {
        Intent intent = new Intent(this, ConverterActivity.class);

        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        changeActivity();
    }
}
