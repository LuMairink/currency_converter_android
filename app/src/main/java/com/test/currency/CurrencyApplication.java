package com.test.currency;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.test.currency.model.ConversionDb;

public class CurrencyApplication extends Application {
    private ConversionDb conversionDb;

    @Override
    public void onCreate() {
        super.onCreate();

        conversionDb = Room.databaseBuilder(getApplicationContext(),
                ConversionDb.class, "conversion_db")
                .fallbackToDestructiveMigration()
                .build();
    }

    public ConversionDb getConversionDb() {
        return conversionDb;
    }

    public void setConversionDb(ConversionDb conversionDb) {
        this.conversionDb = conversionDb;
    }
}
